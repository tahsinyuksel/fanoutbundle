<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 15:41
 */

namespace Ty\FanOutBundle\Event;


class FanOutEvents
{
    /**
     * pre request handler
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const FAN_REQUEST_HANDLER = '.on_request_handler';

    /**
     * completed fan done
     * single and batch process when completed
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const FAN_DONE = '.fan_done';

    /**
     * send new request for fanout
     * continue flow
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     */
    const SEND_REQUEST_HANDLER = '.send_request_handler';
}