<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 14:34
 */

namespace Ty\FanOutBundle\Event;


use Symfony\Component\EventDispatcher\Event;

class FanOutEvent extends Event
{
    private $data;

    /**
     * FanOutEvent constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}