<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/07/18
 * Time: 11:07
 */

namespace Ty\FanOutBundle\Service\Transport;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Ty\FanOutBundle\Model\Adapter\BaseFanAdapter;
use Ty\FanOutBundle\Model\Request\FanRequestInterface;

class DirectTransport implements FanOutTransportInterface
{
    /** @var  ContainerInterface */
    protected $container;

    /**
     * DirectTransport constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function publish($publishName, FanRequestInterface $fanRequest)
    {
        if($this->container->has($publishName)) {
            /** @var BaseFanAdapter $fanOutAdapter */
            $fanOutAdapter = $this->container->get($publishName);
            $fanOutAdapter->setFanRequest($fanRequest);
            $fanOutAdapter->handle();
        }
    }
}