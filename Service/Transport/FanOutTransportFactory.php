<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 17/07/18
 * Time: 17:14
 */

namespace Ty\FanOutBundle\Service\Transport;


use Symfony\Component\DependencyInjection\ContainerInterface;

class FanOutTransportFactory
{
    /** @var  ContainerInterface */
    protected $container;

    protected $transportServices = array(
        'direct'=> 'ty_fan_out.service.transport.transport.direct',
        'queue'=> 'ty_fan_out.service.transport.transport.queue'
    );

    /**
     * FanOutTransportFactory constructor.
     * @param ContainerInterface $container
     * @param array $transportServices
     */
    public function __construct(ContainerInterface $container, $transportServices = array())
    {
        $this->container = $container;
        if(count($transportServices) > 0) {
            $this->transportServices = array_merge($this->transportServices, $transportServices);
        }
    }

    /**
     * @param string $method
     * @return FanOutTransportInterface
     * @throws \Exception
     */
    public function createFactory($method = 'direct')
    {
        $transportName = $method;

        if(isset($this->transportServices[$method])) {
            $transportName = $this->transportServices[$method];
        }

        if($this->container->has($transportName)) {
            return $this->container->get($transportName);
        }

        throw new \Exception('Write Transport not found');
    }
}