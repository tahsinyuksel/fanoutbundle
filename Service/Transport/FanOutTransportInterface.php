<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/07/18
 * Time: 11:08
 */

namespace Ty\FanOutBundle\Service\Transport;


use Ty\FanOutBundle\Model\Request\FanRequestInterface;

interface FanOutTransportInterface
{
    public function publish($publishName, FanRequestInterface $fanRequest);
}