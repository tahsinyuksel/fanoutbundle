<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 02/07/18
 * Time: 15:00
 */

namespace Ty\FanOutBundle\Service\Transport;


use Ty\FanOutBundle\Model\Request\FanRequestInterface;
use Ty\FanOutBundle\Service\Producer\DefaultProducer;

class QueueTransport implements FanOutTransportInterface
{
    /** @var  DefaultProducer */
    protected $producer;

    /**
     * QueueTransport constructor.
     * @param DefaultProducer $producer
     */
    public function __construct(DefaultProducer $producer)
    {
        $this->producer = $producer;
    }

    public function publish($publishName, FanRequestInterface $fanRequest)
    {
        $this->producer->publishToName($publishName, serialize($fanRequest));
    }
}