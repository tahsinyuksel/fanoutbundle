<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 16:45
 */

namespace Ty\FanOutBundle\Service\Transport;


use Ty\FanOutBundle\Model\Request\FanRequestInterface;

class FanOutTransport
{
    /** @var null|FanRequestInterface */
    protected $fanRequest = null;

    /** @var string  */
    protected $publishName = '';

    /** @var FanOutTransportInterface  */
    protected $transport = null;

    /**
     * FanOutTransport constructor.
     * @param FanOutTransportInterface|null $transport
     * @param string $publishName
     */
    public function __construct(
        FanOutTransportInterface $transport = null,
        $publishName = ''
    )
    {
        $this->transport = $transport;
        $this->publishName = $publishName;
    }

    /**
     * @param string $publishName
     * @param FanRequestInterface $fanRequest
     * @return bool
     */
    public function publishToName($publishName, FanRequestInterface $fanRequest)
    {
        $this->setPublishName($publishName);
        $this->setFanRequest($fanRequest);

        return $this->process();
    }

    /**
     * @param FanRequestInterface $fanRequest
     * @return bool
     */
    public function publish(FanRequestInterface $fanRequest)
    {
        $this->setFanRequest($fanRequest);

        return $this->process();
    }

    public function process()
    {
        $this->handle();

        return $this->transport->publish($this->getPublishName(), $this->getFanRequest());
    }

    public function handle()
    {
        if(null == $this->transport) {
            throw new \Exception('Write Transport not found');
        }
    }

    /**
     * @return null|FanRequestInterface
     */
    public function getFanRequest()
    {
        return $this->fanRequest;
    }

    /**
     * @param null|FanRequestInterface $fanRequest
     */
    public function setFanRequest($fanRequest)
    {
        $this->fanRequest = $fanRequest;
    }

    /**
     * @return string
     */
    public function getPublishName()
    {
        return $this->publishName;
    }

    /**
     * @param string $publishName
     */
    public function setPublishName($publishName)
    {
        $this->publishName = $publishName;
    }

    /**
     * @return FanOutTransportInterface
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param FanOutTransportInterface $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

}