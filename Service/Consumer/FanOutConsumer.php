<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/07/18
 * Time: 13:55
 */

namespace Ty\FanOutBundle\Service\Consumer;


use Ty\FanOutBundle\Model\Adapter\BaseFanAdapter;
use Ty\FanOutBundle\Model\Request\FanRequestInterface;

class FanOutConsumer
{
    /** @var BaseFanAdapter */
    protected $fanOutAdapter;

    public function __construct($fanOutAdapter = null)
    {
        $this->fanOutAdapter = $fanOutAdapter;
    }

    public function execute($msg)
    {
        /** @var FanRequestInterface $fanRequest */
        $fanRequest = unserialize($msg);

        $this->fanOutAdapter->setFanRequest($fanRequest);
        $this->fanOutAdapter->handle();
    }

    public function publish($msg)
    {
        $this->execute($msg);
    }

}