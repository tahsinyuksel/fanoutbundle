<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:49
 */

namespace Ty\FanOutBundle\Service\Storage;


use Predis\Client;
use Ty\FanOutBundle\Model\BaseFanModelInterface;
use Ty\FanOutBundle\Model\Storage\BaseFanStorage;

class RedisFanStorage implements BaseFanStorage
{
    /** @var  Client */
    private $redisStore;

    private $name = 'feed_%s';

    private $processMethod = 'batchProcess';

    /**
     * RedisFanStorage constructor.
     * @param $redisStore
     * @param $name
     * @param string $processMethod
     */
    public function __construct($redisStore, $name, $processMethod = 'batchProcess')
    {
        $this->redisStore = $redisStore;
        $this->name = $name;
        $this->processMethod = $processMethod;
    }

    public function batchProcess(array $data, $action = 'add')
    {
        if(count($data) > 0) {

            $this->redisStore->multi();

            /** @var BaseFanModelInterface $model */
            foreach ($data as $model) {
                $this->queryRun($model, $action, 'batchProcess');
            }

            $this->redisStore->exec();
        }
    }

    public function singleProcess(BaseFanModelInterface $model, $action = 'add')
    {
        $this->queryRun($model, $action, 'singleProcess');
    }

    private function queryRun(BaseFanModelInterface $model, $action, $processMethod = 'batchProcess')
    {
        if($this->processMethod != $processMethod) {
            return;
        }

        $key = $this->name;

        $value[$model->getFeedId()] = $model->getRankScore();

        if('remove' == $action) {
            $this->remove(sprintf($key, $model->getToId()), $model->getFeedId());
        } else if('add' == $action) {
            $this->add(sprintf($key, $model->getToId()), $value);
        }
    }

    private function add($key, $value)
    {
        $this->redisStore->zadd($key, $value);
    }

    private function remove($key, $value)
    {
        $this->redisStore->zrem($key, $value);
    }

}