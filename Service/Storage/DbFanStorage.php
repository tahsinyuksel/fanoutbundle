<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 19/07/18
 * Time: 10:40
 */

namespace Ty\FanOutBundle\Service\Storage;


use Doctrine\Common\Persistence\ObjectManager;
use Ty\FanOutBundle\Model\BaseFanModelInterface;
use Ty\FanOutBundle\Model\Storage\BaseFanStorage;

/**
 * Class DbFanStorage
 * @package Ty\FanOutBundle\Service\Storage
 */
class DbFanStorage implements BaseFanStorage
{
    /** @var  ObjectManager */
    private $om;

    private $processMethod = 'batchProcess';

    private $class;

    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $repository;

    private $controlFields = array();

    private $entityList = array();

    /**
     * DbFanStorage constructor.
     * @param ObjectManager $om
     * @param $class
     * @param array $controlFields
     * @param string $processMethod
     */
    public function __construct(ObjectManager $om, $class, $controlFields = array(), $processMethod = 'batchProcess')
    {
        $this->om = $om;
        $this->processMethod = $processMethod;
        $this->class = $class;
        $this->repository = $om->getRepository($class);
        $this->controlFields = is_array($controlFields) ? $controlFields : array();
    }

    public function batchProcess(array $data, $action = 'add')
    {
        if($this->processMethod != 'batchProcess') {
            return;
        }

        if(count($data) == 0) {
            return;
        }

        $hashList = array();

        /** @var BaseFanModelInterface $model */
        foreach ($data as $model) {
            $hashList[] = $this->hashCreate($model);
        }

        $this->entityList = $this->getByHashList($hashList);

        $i = 0;
        /** @var BaseFanModelInterface $model */
        foreach ($data as $model) {
            $this->queryRun($model, $action, $hashList[$i]);
            $i++;
        }

        $this->om->flush();
    }

    private function hashCreate(BaseFanModelInterface $model)
    {
        $hash = '';
        foreach ($this->controlFields as $field) {
            $method = 'get' . ucfirst($field);
            $hash .= $model->$method();
        }
        return md5($hash);
    }

    public function singleProcess(BaseFanModelInterface $model, $action = 'add')
    {
        if($this->processMethod != 'singleProcess') {
            return;
        }

        $hash = $this->hashCreate($model);
        $this->entityList = $this->getByHashList(array($hash));

        $this->queryRun($model, $action, $hash);
        $this->om->flush();
    }

    private function queryRun(BaseFanModelInterface $model, $action, $hash = '')
    {
        if('remove' == $action) {
            foreach ($this->entityList as $item) {
                $this->om->remove($item);
            }
        } else if('add' == $action) {

            // duplicate control
            if(false == $this->isHaveByHash($hash)) {

                $entity = $this->instanceEntity();
                $entity->setFromId($model->getFromId());
                $entity->setFeedId($model->getFeedId());
                $entity->setToId($model->getToId());
                $entity->setRankScore($model->getRankScore());
                $entity->setHash($hash);

                $this->om->persist($entity);
            }
        }
    }

    protected function findRecord(BaseFanModelInterface $model)
    {
        $whereQuery = array();

        foreach ($this->controlFields as $field) {
            $method = 'get' . ucfirst($field);
            $whereQuery[$field] = $model->$method();
        }

        if(count($whereQuery) == 0) {
            return null;
        }

        return $this->repository->findBy($whereQuery);
    }

    private function getByHashList($hashList)
    {
        return $this->repository->findBy(array('hash'=> $hashList));
    }

    private function isHaveByHash($hash)
    {
        $result = false;
        foreach ($this->entityList as $item) {
            if($item->getHash() == $hash) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    private function instanceEntity()
    {
        $name = $this->class;
        return new $name();
    }

}