<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 28/07/18
 * Time: 12:33
 */

namespace Ty\FanOutBundle\Service\Storage;


use Ty\FanOutBundle\Model\BaseFanModelInterface;
use Ty\FanOutBundle\Model\Storage\BaseFanStorage;

class MultiFanStorage implements BaseFanStorage
{
    /**
     * BaseFanStorage items
     * @var array
     */
    protected $fanStorageList = array();

    /**
     * DbRedisFanStorage constructor.
     * @param array $fanStorageList
     */
    public function __construct(array $fanStorageList)
    {
        $this->fanStorageList = $fanStorageList;
    }

    public function batchProcess(array $data, $action = 'add')
    {
        /** @var BaseFanStorage $item */
        foreach ($this->fanStorageList as $item) {
            $item->batchProcess($data, $action);
        }
    }

    public function singleProcess(BaseFanModelInterface $model, $action = 'add')
    {
        /** @var BaseFanStorage $item */
        foreach ($this->fanStorageList as $item) {
            $item->singleProcess($model, $action);
        }
    }

}