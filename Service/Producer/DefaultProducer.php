<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/07/18
 * Time: 13:29
 */

namespace Ty\FanOutBundle\Service\Producer;

use Symfony\Component\DependencyInjection\ContainerInterface;

class DefaultProducer
{
    /** @var  ContainerInterface */
    protected $container;

    /** @var string */
    protected $name = '';

    /** @var object */
    protected $transport = null;

    /**
     * TweetProducer constructor.
     * @param ContainerInterface $container
     * @param string $name
     */
    public function __construct(ContainerInterface $container, $name = '')
    {
        $this->container = $container;
        $this->load($name);
    }

    protected function handle($name)
    {
        if(!empty($name) && $this->container->has($name)) {
            $this->transport = $this->container->get($name);
        }
    }

    /**
     * @param mixed $msg
     */
    public function publish($msg)
    {
        if(null != $this->transport) {
            $this->transport->publish($msg);
        }
    }

    /**
     * @param string $name
     * @param mixed $msg
     */
    public function publishToName($name, $msg)
    {
        $this->load($name);
        $this->publish($msg);
    }

    /**
     * @param string $name
     */
    public function load($name)
    {
        $this->name = $name;
        $this->handle($name);
    }
}