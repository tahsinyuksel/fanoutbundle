<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 16:53
 */

namespace Ty\FanOutBundle\Service\Adapter;


use Ty\FanOutBundle\Model\Adapter\BaseFanAdapter;
use Ty\FanOutBundle\Model\Request\FanRequestInterface;

class FeedFan extends BaseFanAdapter
{

    /**
     * @inheritDoc
     */
    public function handle()
    {
        $this->process();
    }


    /**
     * @inheritDoc
     */
    public function getRankScore($data = null)
    {
        return time();
    }

}