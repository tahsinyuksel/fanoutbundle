<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:39
 */

namespace Ty\FanOutBundle\Service\ToProvider;


use Ty\FanOutBundle\Model\ToProvider\ToProviderAbstract;
use Ty\FanOutBundle\Model\ToProvider\ToProviderInterface;

class DirectToProvider extends ToProviderAbstract implements ToProviderInterface
{
    /**
     * @inheritDoc
     */
    public function getToList($from, $limit = 1000, $offset = 0)
    {
        $data = array(2,3,4);

        $result = array();
        for($i = $offset * $limit; $i < $offset + $limit; $i++) {
            if(isset($data[$i])) {
                $result[] = $data[$i];
            }
        }

        return $this->handleToResponse($result);
    }

}