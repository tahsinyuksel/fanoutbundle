<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 18/07/18
 * Time: 13:55
 */

namespace Ty\FanOutBundle\Service\ToProvider;


use Ty\FanOutBundle\Model\ToProvider\ToProviderAbstract;
use Ty\FanOutBundle\Model\ToProvider\ToProviderInterface;

class HttpToProvider extends ToProviderAbstract implements ToProviderInterface
{
    protected $url;
    
    protected $requestMethod = 'GET';

    protected $secretKey = '';

    /**
     * HttpToProvider constructor.
     * @param $url
     * @param $requestMethod
     * @param string $secretKey
     */
    public function __construct($url, $requestMethod, $secretKey = '')
    {
        $this->url = $url;
        $this->requestMethod = $requestMethod;
        $this->secretKey = $secretKey;
    }

    /**
     * @inheritDoc
     */
    public function getToList($from, $limit = 1000, $offset = 0)
    {
        $newUrl = str_replace(
            array(':offset', ':limit', ':from', ':secret'),
            array($offset, $limit, $from, $this->getSecretKey()),
            $this->url
        );

        $response = $this->callAPI($this->getRequestMethod(), $newUrl);

        if($response && $response != '') {
            $responseArray = json_decode(($response));
            if($responseArray && is_array($responseArray)) {
                return $this->handleToResponse($responseArray);
            }
        }

        return array();
    }

    private function callAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @param string $requestMethod
     */
    public function setRequestMethod($requestMethod)
    {
        $this->requestMethod = $requestMethod;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

}