<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 19/07/18
 * Time: 09:45
 */

namespace Ty\FanOutBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ty\FanOutBundle\Model\BaseFanModel;
use Ty\FanOutBundle\Model\FanModelEntityInterface;

/**
 * FanModelEntity
 *
 * @ORM\Table(name="fan_model_entity")
 * @ORM\Entity(repositoryClass="Ty\FanOutBundle\Repository\FanModelEntityRepository")
 */
class FanModelEntity extends BaseFanModel implements FanModelEntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="fromId", type="integer")
     */
    protected $fromId;

    /**
     * @var int
     *
     * @ORM\Column(name="feedId", type="integer")
     */
    protected $feedId;

    /**
     * @var int
     *
     * @ORM\Column(name="toId", type="integer", nullable=true)
     */
    protected $toId;

    /**
     * @var float
     *
     * @ORM\Column(name="rankScore", type="decimal", nullable=true)
     */
    protected $rankScore;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=true)
     */
    protected $hash;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

}