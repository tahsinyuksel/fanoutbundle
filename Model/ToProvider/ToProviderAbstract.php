<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 14:34
 */

namespace Ty\FanOutBundle\Model\ToProvider;


use Ty\FanOutBundle\Service\ToProvider\ToObject;

abstract class ToProviderAbstract
{
    /**
     * @param array $toList
     * @return array item is BaseToObject
     */
    public function handleToResponse($toList)
    {
        $data = array();

        if(is_array($toList) && count($toList) > 0) {

            foreach ($toList as $item) {
                $to = $this->instanceToObject();

                if(! is_array($item)) {
                    $to->setToId($item);
                } else {
                    $to->setData($item);
                }
                $data[] = $to;
            }
        }

        return $data;
    }

    /**
     * @return BaseToObject
     */
    public function instanceToObject()
    {
        return new ToObject();
    }
}