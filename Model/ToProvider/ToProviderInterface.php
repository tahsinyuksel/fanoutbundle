<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:22
 */

namespace Ty\FanOutBundle\Model\ToProvider;


interface ToProviderInterface
{
    /**
     * Get to list
     * @param mixed $from
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getToList($from, $limit = 1000, $offset = 0);
}