<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 08/09/18
 * Time: 14:15
 */

namespace Ty\FanOutBundle\Model\ToProvider;


abstract class BaseToObject
{
    protected $toId;

    protected $rankScore = 0.0;

    protected $relation = '';

    public function setData($data)
    {
        if(isset($data['id'])) {
            $this->setToId($data['id']);
            $this->setRankScore(isset($data['rankScore']) ? $data['rankScore'] : 0.0);
            $this->setRelation(isset($data['relation']) ? $data['relation'] : '');
        }
    }

    /**
     * @return mixed
     */
    public function getToId()
    {
        return $this->toId;
    }

    /**
     * @param mixed $toId
     */
    public function setToId($toId)
    {
        $this->toId = $toId;
    }

    /**
     * @return mixed
     */
    public function getRankScore()
    {
        return $this->rankScore;
    }

    /**
     * @param mixed $rankScore
     */
    public function setRankScore($rankScore)
    {
        $this->rankScore = $rankScore;
    }

    /**
     * @return mixed
     */
    public function getRelation()
    {
        return $this->relation;
    }

    /**
     * @param mixed $relation
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;
    }
}