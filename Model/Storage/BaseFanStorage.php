<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 23:03
 */

namespace Ty\FanOutBundle\Model\Storage;


use Ty\FanOutBundle\Model\BaseFanModelInterface;

interface BaseFanStorage
{
    public function batchProcess(array $data, $action = 'add');

    public function singleProcess(BaseFanModelInterface $model, $action = 'add');
}