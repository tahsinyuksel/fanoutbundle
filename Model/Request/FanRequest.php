<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 23:12
 */

namespace Ty\FanOutBundle\Model\Request;


class FanRequest implements FanRequestInterface
{
    protected $fromId;

    protected $feedId;

    protected $cursor = 0;

    protected $type;

    protected $data;

    protected $action;

    public function incrementCursor($val)
    {
        $this->cursor += $val;
        return $this->cursor;
    }

    /**
     * @return mixed
     */
    public function getFromId()
    {
        return $this->fromId;
    }

    /**
     * @param mixed $fromId
     */
    public function setFromId($fromId)
    {
        $this->fromId = $fromId;
    }

    /**
     * @return mixed
     */
    public function getFeedId()
    {
        return $this->feedId;
    }

    /**
     * @param mixed $feedId
     */
    public function setFeedId($feedId)
    {
        $this->feedId = $feedId;
    }

    /**
     * @return int
     */
    public function getCursor()
    {
        return $this->cursor;
    }

    /**
     * @param int $cursor
     */
    public function setCursor($cursor)
    {
        $this->cursor = $cursor;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @inheritDoc
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @inheritDoc
     */
    public function setAction($action)
    {
        $this->action = $action;
    }


}