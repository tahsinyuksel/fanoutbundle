<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 23:17
 */
namespace Ty\FanOutBundle\Model\Request;

interface FanRequestInterface
{
    /**
     * @param int $val
     * @return int
     */
    public function incrementCursor($val);

    /**
     * @return mixed
     */
    public function getFromId();

    /**
     * @param mixed $fromId
     */
    public function setFromId($fromId);

    /**
     * @return mixed
     */
    public function getFeedId();

    /**
     * @param mixed $feedId
     */
    public function setFeedId($feedId);

    /**
     * @return mixed
     */
    public function getCursor();

    /**
     * @param mixed $cursor
     */
    public function setCursor($cursor);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param mixed $type
     */
    public function setType($type);

    /**
     * @return mixed
     */
    public function getData();

    /**
     * @param mixed $data
     */
    public function setData($data);

    /**
     * @return string
     */
    public function getAction();

    /**
     * @param string $action
     */
    public function setAction($action);
}