<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:59
 */

namespace Ty\FanOutBundle\Model;


interface FanModelEntityInterface extends BaseFanModelInterface
{
    /**
     * @return string
     */
    public function getHash();

    /**
     * @param string $hash
     */
    public function setHash($hash);
}