<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:59
 */

namespace Ty\FanOutBundle\Model;


abstract class BaseFanModel implements BaseFanModelInterface
{
    protected $fromId;

    protected $feedId;

    protected $toId;

    protected $rankScore;

    /**
     * @return mixed
     */
    public function getFromId()
    {
        return $this->fromId;
    }

    /**
     * @param mixed $fromId
     */
    public function setFromId($fromId)
    {
        $this->fromId = $fromId;
    }

    /**
     * @return mixed
     */
    public function getFeedId()
    {
        return $this->feedId;
    }

    /**
     * @param mixed $feedId
     */
    public function setFeedId($feedId)
    {
        $this->feedId = $feedId;
    }

    /**
     * @return mixed
     */
    public function getToId()
    {
        return $this->toId;
    }

    /**
     * @param mixed $toId
     */
    public function setToId($toId)
    {
        $this->toId = $toId;
    }

    /**
     * @return mixed
     */
    public function getRankScore()
    {
        return $this->rankScore;
    }

    /**
     * @param mixed $rankScore
     */
    public function setRankScore($rankScore)
    {
        $this->rankScore = $rankScore;
    }

}