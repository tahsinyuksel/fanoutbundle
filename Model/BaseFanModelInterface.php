<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 24/06/18
 * Time: 16:59
 */

namespace Ty\FanOutBundle\Model;


interface BaseFanModelInterface
{

    /**
     * @return mixed
     */
    public function getFromId();

    /**
     * @param mixed $fromId
     */
    public function setFromId($fromId);

    /**
     * @return mixed
     */
    public function getFeedId();

    /**
     * @param mixed $feedId
     */
    public function setFeedId($feedId);

    /**
     * @return mixed
     */
    public function getToId();

    /**
     * @param mixed $toId
     */
    public function setToId($toId);

    /**
     * @return mixed
     */
    public function getRankScore();

    /**
     * @param mixed $rankScore
     */
    public function setRankScore($rankScore);

}