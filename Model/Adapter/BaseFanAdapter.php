<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 22/06/18
 * Time: 16:53
 */

namespace Ty\FanOutBundle\Model\Adapter;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Ty\FanOutBundle\Event\FanOutEvents;
use Ty\FanOutBundle\Model\BaseFanModelInterface;
use Ty\FanOutBundle\Model\FanModel;
use Ty\FanOutBundle\Model\Request\FanRequestInterface;
use Ty\FanOutBundle\Model\Storage\BaseFanStorage;
use Ty\FanOutBundle\Model\ToProvider\BaseToObject;
use Ty\FanOutBundle\Model\ToProvider\ToProviderInterface;
use Ty\FanOutBundle\Service\Transport\FanOutTransport;

abstract class BaseFanAdapter
{
    /** @var FanRequestInterface */
    private $fanRequest;

    /** @var int Limit of to list */
    protected $limit = 3;

    /** @var ToProviderInterface */
    protected $toProvider;

    /** @var BaseFanStorage */
    protected $fanStorage;

    /** @var EventDispatcherInterface */
    protected $dispatcher = null;

    /** @var string  */
    protected $eventName = '';

    /** @var FanOutTransport */
    protected $fanOutTransport = null;

    /**
     * BaseFanAdapter constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param BaseFanStorage $fanStorage
     * @param ToProviderInterface $toProvider
     * @param $eventName
     * @param FanOutTransport $fanOutTransport
     * @param int $limit
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        BaseFanStorage $fanStorage,
        ToProviderInterface $toProvider,
        $eventName,
        FanOutTransport $fanOutTransport,
        $limit = 1
    )
    {
        $this->dispatcher = $dispatcher;
        $this->fanStorage = $fanStorage;
        $this->toProvider = $toProvider;
        $this->eventName = $eventName;
        $this->fanOutTransport = $fanOutTransport;
        $this->limit = $limit;
    }

    /**
     * @param FanRequestInterface $fanRequest
     * @param BaseToObject $to
     * @return BaseFanModelInterface
     */
    public function createFanModel($fanRequest, $to)
    {
        $fanModel = new FanModel();
        $fanModelScore = $this->getRankScore(
            array('fanRequest'=> $fanRequest, 'toObject'=> $to)
        );

        $fanModel->setToId($to->getToId());
        $fanModel->setFeedId($fanRequest->getFeedId());
        $fanModel->setFromId($fanRequest->getFromId());

        $fanModel->setRankScore($fanModelScore);

        return $fanModel;
    }

    /**
     * process for fanOut
     * @return mixed
     */
    public function process()
    {
        /**
         * Filters, Detections ex: spam, block
         * Feed Ranking
         * Store add record
         * Publish FanOut
         * Notification or Other Service push
         */
        $i = 0;
        $data = array();

        $this->dispatcher->dispatch(
            $this->getEventName() . FanOutEvents::FAN_REQUEST_HANDLER, new GenericEvent($this->getFanRequest())
        );

        $fanRequest = $this->getFanRequest();

        foreach ($this->getToList($fanRequest->getFromId(), $this->getLimit(), round($fanRequest->getCursor() / $this->getLimit())) as $to) {

            $fanModel = $this->createFanModel($fanRequest, $to);

            $this->singleProcess($fanModel);

            $data[] = $fanModel;
            $i += 1;
        }

        if($i > 0) {

            $this->batchProcess($data);
            $this->dispatcher->dispatch(
                $this->getEventName() . FanOutEvents::FAN_DONE, new GenericEvent($data, array('fanRequest'=> $fanRequest))
            );

            $fanRequest->incrementCursor($i);

            $this->dispatcher->dispatch(
                $this->getEventName() . FanOutEvents::SEND_REQUEST_HANDLER, new GenericEvent($this->getFanRequest())
            );

            // publish continue with cursor
            $this->publish($fanRequest);
        }
    }

    /**
     * Handle process for fanOut
     * @return mixed
     */
    abstract public function handle();

    /**
     * Feed rank score
     * @param mixed|null $data
     * @return float
     */
    abstract public function getRankScore($data = null);

    /**
     * Apply Filters
     * @return bool
     */
    public function applyFilters()
    {
        return true;
    }

    /**
     * Single process per row
     * @param $data
     */
    public function singleProcess($data)
    {
        $this->fanStorage->singleProcess($data, $this->getFanRequest()->getAction());
    }

    /**
     * Batch process for all data
     * @param $data
     */
    public function batchProcess($data)
    {
        $this->fanStorage->batchProcess($data, $this->getFanRequest()->getAction());
    }

    /**
     * Get to ids (follower, following, to etc ids)
     * Ranked
     *
     * @param mixed $from
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getToList($from, $limit = 1000, $offset = 0)
    {
        return $this->toProvider->getToList($from, $limit, $offset);
    }

    /**
     * @return FanRequestInterface
     */
    public function getFanRequest()
    {
        return $this->fanRequest;
    }

    /**
     * @param FanRequestInterface $fanRequest
     */
    public function setFanRequest(FanRequestInterface $fanRequest)
    {
        $this->fanRequest = $fanRequest;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * Publish for continue fanOut
     * @param FanRequestInterface $fanRequest
     */
    public function publish(FanRequestInterface $fanRequest)
    {
        if($fanRequest->getCursor() > 0) {
            $this->fanOutTransport->publish($fanRequest);
        }
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

}