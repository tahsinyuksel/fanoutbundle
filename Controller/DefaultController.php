<?php

namespace Ty\FanOutBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ty\FanOutBundle\Model\Request\FanRequest;

class DefaultController extends Controller
{
    public function indexAction()
    {

        $fanRequest = new FanRequest();
        $fanRequest->setFromId(1);
        $fanRequest->setFeedId(2);
        $fanRequest->setCursor(0);
        $fanRequest->setType('feed');
        $fanRequest->setAction('add');

//        $producer = $this->container->get('ty_fan_out.service.mq.fan_out_producer');
//        $producer->publishToName("ty_fan_out.service.mq.consumer.fan_out_consumer", serialize($fanRequest));

        $fanWriteService = $this->container->get('ty_fan_out.service.transport.fan_out_transport');
        $fanWriteService->publish($fanRequest);

        exit('-end-');
//        return $this->render('TyFanOutBundle:Default:index.html.twig');
    }
}
