<?php
/**
 * Created by PhpStorm.
 * User: jokerdev1
 * Date: 29/06/18
 * Time: 14:17
 */

namespace Ty\FanOutBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class FanOutEventListener implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return array(
            'ty_feed_fan.fan_done'=> 'onFanDone',
            'ty_feed_fan.send_request_handler'=> 'sendRequestHandler',
            'ty_feed_fan.on_request_handler'=> 'onRequestHandler',
        );
    }

    public function onFanDone(GenericEvent $event)
    {
        
    }

    public function onRequestHandler(GenericEvent $event)
    {

    }

    public function sendRequestHandler(GenericEvent $event)
    {

    }

}